﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using MimeKit;

namespace FactoriesTestFacility
{
    class Program
    {
        private static readonly IServiceProvider container;
        

        static Program()
        {
            container = ServicesConfigurator.BuildContainer();
      
        }
        static void Main(string[] args)
        {
           List<string> adresses = new List<string>()
           {
               "ib.ra.2000@bk.ru",
               "gevorg.kesyan@mail.ru"
           };
            

            container.GetService<EmailSender<MimeMessage>>().SendEmail(adresses, "Привет!", "Лабораторная работа готова!");
        }

    }
}

