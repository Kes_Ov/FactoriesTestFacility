﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using FactoriesTestFacility.Classes;
using MimeKit;
using FactoriesTestFacility.Interfaces;
using FactoriesTestFacility;

namespace FactoriesTestFacility
{
    class ServicesConfigurator
    {
        private readonly IServiceCollection services =new ServiceCollection();

        public static IServiceProvider BuildContainer()
            => new ServicesConfigurator().services.BuildServiceProvider();

        private ServicesConfigurator()
        {
            RegisterServices();
        }

        private void RegisterServices()
        {
            services.AddSingleton<IEmailSender<MimeMessage>, MailKitMessageSender>();
            services.AddTransient<IEmailMessage<MimeMessage>, MimeMessageDummy>();
            services.AddSingleton<IMessageFactory<MimeMessage>, MimeMessageFactory>();
            services.AddSingleton<IMailSenderFactory<MimeMessage>, MimeMessageSenderFactory>();
            services.AddSingleton<EmailSender<MimeMessage>>();

        }
    }
}
