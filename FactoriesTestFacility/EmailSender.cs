﻿using System;
using System.Collections.Generic;
using System.Text;
using FactoriesTestFacility.Classes;
using FactoriesTestFacility.Interfaces;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;

namespace FactoriesTestFacility
{
    class EmailSender<TMessageType>
    {
        private readonly IMailSenderFactory<TMessageType> senderFactory;
        private readonly IMessageFactory<TMessageType> messageFactory;

        public void SendEmail(List<string> adresses, string subj, string message)
        {
            //smtp.Connect("smtp.yandex.com",465,true);
            //smtp.Authenticate("phzx", "blloimaryrlbqofq");
            foreach (var t in adresses)
            {
                senderFactory.Create().SendMessage(messageFactory.Create("gevkes@yandex.ru", t, subj, message));
                //smtp.Send(messageFactory.Create("gevorgkesyan@gmail.com", t, "ping", "привет. проверка связи."));
            }
            //smtp.Disconnect(true);
        }

        public EmailSender(IMailSenderFactory<TMessageType> senderFactory, IMessageFactory<TMessageType> messageFactory)
        {
            this.messageFactory = messageFactory ?? throw new System.ArgumentNullException(nameof(messageFactory));
            this.senderFactory = senderFactory ?? throw new System.ArgumentNullException(nameof(senderFactory));
        }


    }
}
