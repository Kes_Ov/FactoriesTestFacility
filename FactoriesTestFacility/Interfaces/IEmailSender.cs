﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoriesTestFacility.Interfaces
{
    interface IEmailSender<TMessageType>
    {
        void SendMessage(IEmailMessage<TMessageType> message);
    }
}
