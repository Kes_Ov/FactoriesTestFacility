﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoriesTestFacility.Interfaces
{
    interface IMailSenderFactory<TMessageType>
    {
        IEmailSender<TMessageType> Create();
    }
}
