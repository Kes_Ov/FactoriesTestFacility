﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoriesTestFacility.Interfaces
{
    interface IEmailMessage<TMessageType>
    {
        TMessageType Message { get; set; }
    }
}
