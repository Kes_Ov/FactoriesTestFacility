﻿using System;
using System.Collections.Generic;
using System.Text;
using FactoriesTestFacility.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using MimeKit;

namespace FactoriesTestFacility.Classes
{
    class MimeMessageSenderFactory: IMailSenderFactory<MimeMessage>
    {
        private readonly IServiceProvider container;

        public MimeMessageSenderFactory(IServiceProvider container)
        {
            this.container = container ?? throw new System.ArgumentNullException(nameof(container));
        }

        public IEmailSender<MimeMessage> Create()
        {
            return container.GetService<IEmailSender<MimeMessage>>();
        }
    }
}
