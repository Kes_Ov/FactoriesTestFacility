﻿using System;
using System.Collections.Generic;
using System.Text;
using FactoriesTestFacility.Interfaces;
using MimeKit;

namespace FactoriesTestFacility.Classes
{
    class MimeMessageDummy: IEmailMessage<MimeMessage>
    {
        private MimeMessage message = new MimeMessage();

        public MimeMessage Message
        {
            get => message;
            set => message=value;
        }
    }
}
