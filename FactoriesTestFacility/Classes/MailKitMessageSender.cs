﻿using System;
using System.Collections.Generic;
using System.Text;
using FactoriesTestFacility.Interfaces;
using MailKit.Net.Smtp;
using MimeKit;

namespace FactoriesTestFacility.Classes
{
    class MailKitMessageSender: IEmailSender<MimeMessage>
    {
        private static SmtpClient smtp = new SmtpClient();

        static MailKitMessageSender()
        {
            smtp.Connect("smtp.yandex.com", 465, true);
            smtp.Authenticate("gevkes@yandex.ru", "45825243");
        }
        public void SendMessage(IEmailMessage<MimeMessage> message)
        {
            smtp.Send(message.Message);
        }

        ~MailKitMessageSender()
        {
            smtp.Disconnect(true);
        }
    }
}
