﻿using System;
using System.Collections.Generic;
using System.Text;
using FactoriesTestFacility.Interfaces;
using MimeKit;
using MimeKit.Text;

namespace FactoriesTestFacility.Classes
{
    class MimeMessageFactory: IMessageFactory<MimeMessage>
    {
        private readonly IEmailMessage<MimeMessage> blankmessage;
        public MimeMessageFactory(IEmailMessage<MimeMessage> blankmessage)
        {
            this.blankmessage = blankmessage ?? throw new System.ArgumentNullException(nameof(blankmessage));
        }
        public IEmailMessage<MimeMessage> Create() => blankmessage;
        public IEmailMessage<MimeMessage> Create(string from, string to, string subj, string body)
        {
            MimeMessage message = new MimeMessage();
            message.From.Add(InternetAddress.Parse(from));
            message.To.Add(InternetAddress.Parse(to));
            message.Subject = subj;
            message.Body = new TextPart(TextFormat.Plain){Text = body};
            return blankmessage;
        }
    }
}
